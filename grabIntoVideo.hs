

import System.Process
import System.Directory
import System.Environment
import Data.Maybe
import Data.List


framesDir = "./1"
batchN :: Int
batchN = 300
nameBase = "o"
ext = ".png"
command _1 _2 _3 = "ffmpeg -start_number " 
            ++ _1 
            ++ " -i " ++ framesDir ++ "/" ++ nameBase ++ "%d" ++ ext 
            ++ " -c:v libx265 -b:v 2000k -y -r 30 -s 1920x1080 -threads 1 " 
            ++ "-vframes " ++ _2 ++ " "
            ++ _3


renderFrame f = "palemoon http://localhost:8000/" ++ f



main = do
   [s,m]<-getArgs
   case (read s,m) of
      (x,"go")     -> do 
                        applyStops
                        monitorDir x
                        
      (x,"render") ->   monitorDir x
      (x,"report") -> do 
                        _<-reportMissing x
                        return ()
      (x,"fix")    -> do 
                        m<-reportMissing x
                        fixMissing x m
      (x,"test")    -> do 
                        m<-reportMissing x
                        fixIfHasMissing x m
   


   
   
--listDirectory framesDir


monitorDir start = do
   putStrLn $ show start
   l<-listDirectory framesDir
   runCommandIfHasBatch start batch $ hasBatch l $ batch
   
   where
   batch = map toFilename [start, start+1 .. start+batchN-1]


toFilename n = nameBase ++ (show n) ++ ext
toFilename1 n = framesDir ++ "/" ++ nameBase ++ (show n) ++ ext
toFilenameHTML n = framesDir ++ "/" ++ nameBase ++ (show n) ++ ".html"
toFilenameSrv n = nameBase ++ (show n) ++ ".html"

hasBatch l b = and $ map (\f-> elem f l) b

--hasMissing :: [FilePath] -> [(String,String,String)]-> [(String,String,String)]
hasMissing l b = filter (\(fs,f1,f,fh)-> isNothing $ find (isElem f) l) b
   where
   --_2 (_,f1,_) = f1
   isElem f1 f = f==f1

hasListed l b = filter (\(fs,f1,f,fh)-> isJust $ find (isElem f) l) b
   where
   --_2 (_,f1,_) = f1
   isElem f1 f = f==f1


reportMissing start = do
   putStrLn $ show start
   l<-listDirectory framesDir
   putStrLn $ unlines $ map _x $ hasMissing l batch
   return $ hasMissing l batch
   where
      batch = missingBatch start
      _x (_,x,_,_) = x
   
   
missingBatch start = zip4 (map toFilenameSrv  $ missingSequence start) 
                          (map toFilename1    $ missingSequence start)
                          (map toFilename     $ missingSequence start)
                          (map toFilenameHTML $ missingSequence start)
missingSequence start = [start, start+1 .. start+batchN-1]


runFixMissing m = mapM_ (\(fs,f1,f,fh) -> do 
                                             applyStop fh 
                                             runRender fs f1
                                             sleep 5) m 


fixMissing start m = do
  -- m<-reportMissing start
   runFixMissing m 

fixIfHasMissing :: Int -> [(String,String,String,String)] -> IO ()
fixIfHasMissing start m = do
   --print m
   --print hadRunFullLength
   --print $ hasListed [fstart,fend] m -- $ hasMissing [fstart,fend] m
   case hadRunFullLength of
      False -> do 
                  putStrLn ("has " ++ (show $ length m) ++ " files to go")
                  --return "no go"
      True  -> do 
                  runFixMissing m
                  --return "fire"
   where
      end = start+batchN-1
      fstart = toFilename start
      fend   = toFilename end
      hadRunFullLength = (\l-> l==[]) 
                           $ hasListed [fstart,fend] m
      
      
{-fixIfHasMissingAndContinue start = do 
   m<-reportMissing start
   c<-fixIfHasMissing start m
   case c of
      "no go" -> monitorDir start
      "fire"  -> do 
                     sleep 60
                     m<-reportMissing start
 -}                    
   


runCommandIfHasBatch start batch True = do
   (runCommand $ command s e ("t" ++ s ++ ".mkv")
      )>>= waitForProcess
      
   mapM_ removeFile $ map (\f-> framesDir ++ "/" ++ f) batch
   
   monitorDir (start+batchN)
   
   where
   s= show   start
   --e= show $ start+batchN
   e= show batchN

runCommandIfHasBatch start _ False = do
   runRender (toFilenameSrv start) (toFilename1 start)
   sleep 60
   
   m<-reportMissing start
   fixIfHasMissing start m
   
   monitorDir start
   

runRender fs f1  = do 
   e<-doesFileExist f1
   case e of
      True -> putStrLn $ "runRender file exists: " ++ f1
      False -> do 
                  putStrLn $ "runRender for: " ++ fs
                  (runCommand $ renderFrame fs
                     ) -- >>= waitForProcess
                  return ()





sleep t = (runCommand $ "sleep " ++ (show t)
            )>>= waitForProcess

disableGoNext s = unlines $ insert t' "" ""
                     $ span (/= t) $ lines s 
   where
   t  = "            setTimeout(goNext, 250);"
   t' = "//            setTimeout(goNext, 250);"
   
   insert n xl xr (l,r) = concat [l
                                 ,(concat [xl , n , xr]) : (drop 1 r)
                                 ]
   
stopFrames = [batchN, batchN+batchN ..]

   
applyStop f = do  putStrLn $ "applyStop to: " ++ f
                  s<-readFile f
                  length s `seq` (writeFile f $ disableGoNext s)
   --writeFile f $ disableGoNext s

existingFrames [] = return []
existingFrames (f:rest) = do 
   e<- doesFileExist f
   case e of 
      False -> do 
                  --putStrLn $ "file does not exist: " ++ f
                  return []
      True  -> do 
                  --putStrLn $ "applyStop to: " ++ f
                  --apply f
                  r<-existingFrames rest
                  return $ f:r

applyStops = do 
   e<-existingFrames $ map toFilenameHTML stopFrames
   mapM_ applyStop e


