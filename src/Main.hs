-----------------------------------------------------------------------------
--
-- Module      :  Main
-- Copyright   :
-- License     :  PublicDomain
--
-- Maintainer  :
-- Stability   :
-- Portability :
--
-- |
--
-----------------------------------------------------------------------------
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Main (

main

) where

import System.IO

import System.Environment

import Data.List
import Data.DList hiding (tail,map,empty,fromList,head,concat,lookup,foldr)
import qualified Data.DList as DL

import Data.Maybe
import Data.Word

import Data.Aeson
import Data.Aeson.Lens

import GHC.Generics (Generic, Generic1)

import Control.Monad.RWS.Lazy
import Control.Monad.Except
import Control.Monad.Extra

import Control.Monad.State hiding (put, get)
import qualified Control.Monad.State as St


import Control.Lens hiding ((.=),Context,(|>))
import qualified Control.Lens as L --hiding (element)

import Data.Either

import Data.Map.Syntax ((##))
import qualified Data.Text as T
import qualified Data.ByteString as B
import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as B8
import Blaze.ByteString.Builder
import Text.Printf (printf)
import Heist
import Heist.Interpreted --(renderTemplate)
import Codec.Picture
import qualified Codec.Picture as CPic
import System.FilePath.Posix (takeDirectory, takeFileName )
import System.Directory (copyFile,removeFile)

import Snap.Http.Server (quickHttpServe)
import Snap.Core (Snap, ifTop, route)
import Snap.Util.FileServe (serveDirectory)

--import qualified Data.Map as DMap
--import Text.ParserCombinators.Parsec
--import Text.ParserCombinators.Parsec.Char
--import System.Process

--import Control.DeepSeq


data AnimationLayoutType = A1|A1C|A2|A3|A4 deriving (Eq,Show,Read, Generic)


data Config =
   Cf { 
       _outputWidth :: Word
      ,_outputHeight :: Word
      ,_files :: [(String,(Float,Float))]
      ,_verbosity ::[String]
      ,_outputFileBase :: String
      ,_duration :: [(Word,(AnimationLayoutType,Word))]
      ,_role :: String
      ,_siteLink :: String
     }deriving (Show, Generic)
L.makeLenses ''Config

instance FromJSON Config
instance FromJSON AnimationLayoutType

data Context =
   C { 
       _cverbosity ::[String]
      ,_outputFile :: String
      ,_heiststate :: HeistState IO 
     } -- deriving (Show)
L.makeLenses ''Context

data ApplicationLog =
    OtherError       String
   |Note             String
   deriving (Show)




--type A' m = ExceptT ApplicationLog (RWST Config (DList ApplicationLog) Context m)
--type A = A' Identity
--type AIO = A' IO

newtype AT m a = AT {
   runATma :: ExceptT ApplicationLog (RWST Config (DList ApplicationLog) Context m ) a
   } deriving (Functor, Applicative, Monad, MonadReader Config, MonadState Context
               , MonadIO)



runAT :: Monad m  => AT m a 
                     -> Config 
                     -> Context 
                     -> m (Either ApplicationLog a,Config,Context,DList ApplicationLog)
runAT a cf c =  do
   (u,c1,l)<-runRWST (runExceptT $ runATma a) cf c
   return (u,cf,c1,l)
   



data AnimHTML = AnimHTML
   {
      _page   :: T.Text
     ,_width  :: T.Text
     ,_height :: T.Text
     ,_anim   :: T.Text
     ,_anim1  :: T.Text
     ,_pagesL :: [T.Text]
     ,_pagesR :: [T.Text]
   }
L.makeLenses ''AnimHTML 

emptyState1HTML = AnimHTML
   {
      _page   = ""
     ,_width  = "1920px"
     ,_height = "1080px"
     ,_anim   = "0%"
     ,_anim1  = "0%"
     ,_pagesL = []
     ,_pagesR = []
   }


heistConfig =
  (set hcNamespace "") 
    $ (set hcInterpretedSplices defaultInterpretedSplices) 
    $ (set hcLoadTimeSplices defaultLoadTimeSplices) 
    $ (set hcTemplateLocations [loadTemplates "tpls"]) 
    $ emptyHeistConfig
    


layoutCommon_c :: String -> AnimHTML -> ByteString -> AT IO ByteString
layoutCommon_c p s tpl = do
   cf<-ask
   c<- get
   let ow = fromIntegral $ cf^.outputWidth
   let oh = fromIntegral $ cf^.outputHeight
   --let anim = slidePrecentage*100
   let heistState = c^.heiststate
   
   --init<- liftIO $ initHeist heistConfig
   --let heistState = either (error "oops") id init
   
   rt <- liftIO $ renderTemplate 
       (bindSplices 
         (evalState 
            a3Splice
            (set page (T.pack p)
               $ set width (T.pack $ show ow)
               $ set height (T.pack $ show oh)
               $ s
            --   $ emptyState1HTML
            )
            
         ) 
         heistState
      ) 
      tpl

   return $ toByteString $ maybe (error "oops") fst rt
   

layoutCommon1 :: String -> Float -> ByteString -> AT IO ByteString
layoutCommon1 p a tpl = do
   layoutCommon_c p s tpl
   where
   s = set anim (T.pack $ (printf "%.2f" a) ++ "%") emptyState1HTML

layoutCommon2 :: String -> Float -> Float -> ByteString -> AT IO ByteString
layoutCommon2 p a a1 tpl = do
   layoutCommon_c p s tpl
   where
   s = set anim (T.pack $ (printf "%.2f" a) ++ "%") 
         $ set anim1 (T.pack $ (printf "%.2f" a1) ++ "%") emptyState1HTML
   

layoutCommon3 :: [String] -> [String] -> Float -> ByteString -> AT IO ByteString
layoutCommon3 pl pr a tpl = do
   layoutCommon_c "" s tpl
   where
   s = set anim (T.pack $ (printf "%.2f" a) ++ "%") 
         $ set pagesL (map T.pack $ pl) 
         $ set pagesR (map T.pack $ pr) emptyState1HTML
   



   
{--namePages :: HeistState IO -> String -> ByteString -> IO ByteString
namePages heistState p tpl = do
   
   rt <- renderTemplate 
       (bindSplices 
         ("o"  ## textSplice $ T.pack p
         ) 
         heistState) 
       tpl

   return $ toByteString $ maybe (error "oops") fst rt
--} 
  
namePages :: ByteString -> String -> ByteString -> IO ByteString
namePages name page next = do
   p<- B8.readFile page
   return
      $ B8.unlines
      $ insert next nl' nr
      $ span (/= n)
      $ insert name ol or
      $ span (/= o) 
      $ B8.lines p
   where
      o        = "         saveAs(blob, \"${o}\" );"
      (ol,olr) = B8.span      (/= '$') o
      or       = B8.dropWhile (/= '"') olr
   
      n        = "         window.location.href = \"http://localhost:8000/${next}\";"
      (nl,nlr) = B8.span      (/= '$') n
      nr       = B8.dropWhile (/= '"') nlr
      nl'      = B8.concat [B8.takeWhile (/= '"') nl, "\""]
   
      insert n xl xr (l,r) = concat [l
                                    ,(B8.concat [xl , n , xr]) : (drop 1 r)
                                    ]
   
   
   
   
   
   
a1_2_4Splice :: State AnimHTML (Splices (Splice IO))
a1_2_4Splice = do 
   mph <- St.get
   return $ mconcat $ [
        "page"  ## textSplice $ _page   mph
       ,"w"     ## textSplice $ _width  mph
       ,"h"     ## textSplice $ _height mph
       ,"anim"  ## textSplice $ _anim   mph 
       ,"anim1" ## textSplice $ _anim1  mph
       ]
   

a3Splice :: State AnimHTML (Splices (Splice IO))
a3Splice = do 
   mph <- St.get
   return $ mconcat $ concat [[
        "page"  ## textSplice $ _page   mph
       ,"w"     ## textSplice $ _width  mph
       ,"h"     ## textSplice $ _height mph
       ,"anim"  ## textSplice $ _anim   mph 
       ,"anim1" ## textSplice $ _anim1  mph
       ]
       ,pagesLeft  (mph^.pagesL) 
       ,pagesRight (mph^.pagesR)
       ]

   where
      perPage (n,p) = (T.concat [T.pack "page", T.pack $ show n])  ## textSplice p
      pagesLeft l = map perPage $ zip _1andSoOn $ reverse l
      pagesRight r = map perPage $ zip [6,7..] r
      
      _1andSoOn :: [Int]
      _1andSoOn =  [5,4..1]
   
   
   
   
layout1Animation :: (String,(Float,Float)) -> Float -> Float -> AT IO ByteString
layout1Animation (p,(pw,ph)) duration time = do   
   cf<-ask
   
   let ow = fromIntegral $ cf^.outputWidth
   let oh = fromIntegral $ cf^.outputHeight
   
   
   let pwfactor = ow / pw
   let ph' = ph*pwfactor
   
   let anim = - ((translateAxis oh ph')/oh) * slidePrecentage*100
   
   layoutCommon1 p anim "a1"
   where
      slidePrecentage = time / duration
      
        
   
layout1Animation' :: (String,(Float,Float)) -> Float -> Float -> AT IO ByteString
layout1Animation' img@(p,(pw,ph)) duration time = do   
   cf<-ask
   
   let ow = fromIntegral $ cf^.outputWidth
   let oh = fromIntegral $ cf^.outputHeight
   
   case (pw>ph) of
      True  -> case (time<=(duration/2)) of 
                  True-> layout7Animation img (duration/2) time
                  False-> layout1Animation img (duration/2) ( time - (duration/2) )
      False -> layout1Animation img duration time
   
   
      
       

layout6_7Animation_c :: (String,(Float,Float)) -> Float -> Float -> ByteString -> AT IO ByteString
layout6_7Animation_c (p,(pw,ph)) duration time tpl = do   
   cf<-ask
   
   let ow = (fromIntegral $ cf^.outputWidth)  
   let oh = (fromIntegral $ cf^.outputHeight) 
   
   
   let pwfactor = ow / pw
   let ph' = ph*pwfactor
   
   let anim = - ((translateAxis oh (ph'*2))/oh) * slidePrecentage*100
   
   layoutCommon1 p anim tpl
   where
      slidePrecentage = time / duration

layout6Animation :: (String,(Float,Float)) -> Float -> Float -> AT IO ByteString
layout6Animation img@(p,(pw,ph)) duration time = layout6_7Animation_c img duration time "a6"

layout7Animation :: (String,(Float,Float)) -> Float -> Float -> AT IO ByteString
layout7Animation img@(p,(pw,ph)) duration time = layout6_7Animation_c img duration time "a7"

   
layout2Animation :: (String,(Float,Float)) -> Float -> Float -> AT IO ByteString
layout2Animation img@(p,(pw,ph)) duration time = do
   cf<-ask
   
   let ow = fromIntegral $ cf^.outputWidth
   let oh = fromIntegral $ cf^.outputHeight
   
   case (pw>ph) of
      True  -> layout1Animation img duration time
      False -> do
                  let phfactor = oh / ph
                  let pwl = pw * phfactor
                  let pwr = ow - pwl
                  let pwrFactor = pwr/pw
                  let phr = ph*pwrFactor
                  
                  let anim = - ((translateAxis oh phr)/oh) * slidePrecentage*100
                  
                  layoutCommon1 p anim "a2"
   
   where
      slidePrecentage = time / duration

translateAxis oa pa 
   |pa>oa = pa - oa
   |otherwise = 0
      

layout4Animation :: (String,(Float,Float)) -> Float -> Float -> AT IO ByteString
layout4Animation img@(p,(pw,ph)) duration time = do
   cf<-ask
   
   let ow = fromIntegral $ cf^.outputWidth
   let oh = fromIntegral $ cf^.outputHeight
   
   case (pw>ph) of
      True  -> layout6Animation img duration time
      False -> do
                  let phfactor = oh / ph
                  let pwl = pw * phfactor
                  let pwr = ow - pwl
                  let pwrFactor = pwr/pw
                  let phr = ph*pwrFactor
                  
                  let anim = 100 - slidePrecentage*100
                  let anim1 = (
                                 ((translateAxis oh phr)/oh) * slidePrecentage
                                 - ((translateAxis oh phr)/oh)
                              )*100
                  
                  layoutCommon2 p anim anim1 "a4"
   where
   slidePrecentage = time / duration



layout3Animation :: [(String,(Float,Float))] 
                     -> (String,(Float,Float))
                     -> [(String,(Float,Float))] 
                     -> Float 
                     -> Float 
                     -> AT IO ByteString
layout3Animation pl (_,(pw,ph)) pr duration time = do
   cf<-ask
   
   let ow = fromIntegral $ cf^.outputWidth
   let oh = fromIntegral $ cf^.outputHeight


   let anim = 100 - slidePrecentage*100
   
   case (pw>ph) of
      True  -> runLayoutCommon3 anim "a5"
      False -> runLayoutCommon3 anim "a3"

   where
   slidePrecentage = time / duration
   
   runLayoutCommon3 anim tpl = layoutCommon3 (map fst pl)
                                             (map fst pr)
                                             anim 
                                             tpl



imgResolution (ImageRGB8   (Image {imageWidth  = pw
                                  ,imageHeight = ph})) = Just (pw,ph)
imgResolution (ImageRGB16  (Image {imageWidth  = pw
                                  ,imageHeight = ph})) = Just (pw,ph)
imgResolution (ImageYCbCr8 (Image {imageWidth  = pw
                                  ,imageHeight = ph})) = Just (pw,ph)
imgResolution (ImageY8     (Image {imageWidth  = pw
                                  ,imageHeight = ph})) = Just (pw,ph)
imgResolution _                 = Nothing







{-- ================================================================================================
================================================================================================ --}
loadImage :: FilePath -> IO (Maybe (CPic.DynamicImage))--(Maybe (CPic.Image CPic.PixelRGB8))
loadImage name = do image <- CPic.readImage name
                    case image of
                      (Left s) -> do
                                    print s
                                    return Nothing
                                     --exitWith (ExitFailure 1)
                      (Right d) ->
                                 do
                                    return $ Just d -- $ fmt d
                                 --return  $ Just $ CPic.pixelAt ((\(CPic.ImageRGB8 i) -> i) d) 0 0
                                 --return $ Just d
  where
  fmt :: CPic.DynamicImage -> Maybe (CPic.Image CPic.PixelRGB8)
 -- fmt i
 --   |(CPic.ImageRGB8 i) = Just i
 --   |otherwise = Nothing
  fmt (CPic.ImageRGB8 i) = Just i
  fmt (_) = Nothing

       --(Maybe CPic.PixelRGB8) --(Maybe CPic.DynamicImage)
----------------------------------------------------------------------------------------------------















runMake :: AT IO [ByteString]
runMake = do
   cf<- ask
   
   --init<- liftIO $ initHeist heistConfig
   
   
   let il = cf^.files
   
   let ps =              il
   let ls = tail $ inits il
   let rs = tail $ tails il
   
   concatMapM (\(l,p,r)-> page l p r) $ zip3 ls ps rs
   
   where
   page pl p pr = do
   
      a2<-mapM (layout2Animation p 150 ) [1,2.. 150]
      a4<-mapM (layout4Animation p 150 ) [1,2.. 150] 
      a1<-mapM (\t-> layout1Animation' p 300 t) [1,2.. 300] 

      case pr of
         []->     return $ concat [a2,a4,a1]
         r -> do
                  let plv = drop ((length pl)-5) pl
                  let prv = take 5 r
                  a3<-mapM (layout3Animation plv (head r) prv 300 ) [1,2.. 300]
                  return $ concat [a2,a4,a1,a3]
         
   
   
   --i<- loadImageAT $ head $ cf^.files
   --let ip = fromJust $ fromDynamicImage $ fromJust i

   --mapM (layout2Animation ip 30 ) [1,2.. 30]
   
   {-il<- mapM loadImageAT $ cf^.files
   pl<- stackPicturesVertical 
         $ map (fromJust . fromDynamicImage . fromJust) 
         $ take (div (length il) 2) il 
         
   pr<- stackPicturesVertical 
         $ map (fromJust . fromDynamicImage . fromJust) 
         $ drop (div (length il) 2) il 
   
   
   a1c<-mapM (\t-> layout1AnimationCommon pl 30 t 640 720 ) [0,1.. 30]
   
   a1<-mapM (\t-> layout1Animation pl 30 t) [0,1.. 30]
     
   a2<-mapM (layout2Animation ip 30 ) [1,2.. 30]  
         
   a3<-mapM (layout3Animation pl ip pr 30 ) [1,2.. 30]
   
   return $ concat [a1c,a1,a2,a3]
-}


site :: String -> Snap ()
site d =
    --ifTop (serveDirectory d)
    route [("/", serveDirectory d)]
    

serve cf = do
   liftIO $ quickHttpServe $ site $ takeDirectory $ cf^.outputFileBase

ifServe "srv" cf = serve cf
ifServe _ _ = return ()



ifGenerator "gen" cf = do
   --hs<- liftIO $ initHeist heistConfig
   hs <- either (error "oops") id <$> initHeist heistConfig
   let c = C { 
       _cverbosity = cf^.verbosity
      ,_outputFile = cf^.outputFileBase
      ,_heiststate = hs
     }
     
   

   --runAT runMake cf c
   
   let nFrames = map show [1,2.. fromIntegral $ (length $ cf^.files)
                              *(fromIntegral $ sum $ map (snd.snd) $ cf^.duration)] 
   
   let fFrames = map (\n-> cf^.outputFileBase ++ n ++ ".tpl") nFrames
   let htmlFrames = map (\n-> cf^.outputFileBase ++ n ++ ".html") nFrames
   --let tplFrames = map (\n-> B8.pack ( (takeFileName $ cf^.outputFileBase) ++ n)) nFrames
   let pngFrames = map (\n-> B8.pack ( (takeFileName $ cf^.outputFileBase) ++ n ++ ".png")) nFrames
   
   
   
   (Right frames, _, _, _)<-runAT runMake cf c
   mapM_ (\(p,n)-> do
                     putStrLn    n
                     B.writeFile n p
         )
         $! zip frames
                fFrames
   
   
   putStrLn $ takeDirectory $ cf^.outputFileBase
   
   os <- mapM (\(n,p,h)-> namePages n p $ B8.concat [B8.pack $ cf^.siteLink, h]) 
            $ take (length frames) 
            $ zip3 pngFrames 
                   fFrames 
                   $ map B8.pack $ concat [map takeFileName $ drop 1 htmlFrames, [""]]
   
   mapM_ (\(f,s)-> do 
                     putStrLn f
                     B.writeFile f s) $ zip htmlFrames os
   
ifGenerator _ _ = return ()





main = do
   a<-getArgs
   j<- readFile $ unwords a
   f<- (return . lines) =<< (readFile $ fromMaybe "files" $ j ^? key "files"  . _JSON)
   r<- mapM (\p-> (return 
                     . (\(l,r)->(fromIntegral l, fromIntegral r)) 
                     . fromJust 
                     . imgResolution 
                     . fromJust
                  )
                  =<< (liftIO $ loadImage p)
            )
            f
   
   
   let cf = Cf { 
       _outputWidth = fromMaybe 1280 $ j ^? key "outputWidth"  . _JSON
      ,_outputHeight = fromMaybe 720 $ j ^? key "outputHeight"  . _JSON
      ,_outputFileBase = fromMaybe "o" $ j ^? key "outputFileBase"  . _JSON
      ,_files = zip f r
      ,_verbosity = fromMaybe [] $ j ^? key "verbosity"  . _JSON
      ,_duration = maybe [(1,(A2,150))
                          ,(2,(A4,150))
                          ,(3,(A1,300))
                          ,(4,(A3,300))]
                     (read.T.unpack)
                      $ j ^? _Value . key "duration"  . _JSON
      ,_role = fromMaybe "srv" $ j ^? key "role"  . _JSON
      ,_siteLink = fromMaybe "localhost" $ j ^? key "site"  . _JSON
     }
   
   ifGenerator (cf^.role) cf
   ifServe     (cf^.role) cf
   



    
