{-# LANGUAGE TemplateHaskell #-}

import System.Process
import System.Directory
import System.Environment
import Data.Maybe
import Data.List

import Control.Monad.State.Strict hiding (put, get)
import qualified Control.Monad.State.Strict as St

import Control.Lens hiding ((.=),Context,(|>))
import qualified Control.Lens as L --hiding (element)



framesDir = "./1"
tmpfsDir  = "./tmpfs"
batchN :: Int
batchN = 300
nameBase = "o"
ext = ".png"
command _1 _2 _3 = "ffmpeg -start_number " 
            ++ _1 
            ++ " -i " ++ framesDir ++ "/" ++ nameBase ++ "%d" ++ ext 
            ++ " -c:v libx265 -b:v 2000k -y -r 30 -s 1920x1080 -threads 1 " 
            ++ "-vframes " ++ _2 ++ " "
            ++ _3


renderFrame f = "palemoon http://localhost:8000/" ++ f

data RenderContext = RenderContext {
    _f   :: String
   ,_fs  :: String
   ,_fd  :: String
   ,_fdh :: String
   ,_ft  :: String
   ,_waitRetriesLeft :: Int
   }
L.makeLenses ''RenderContext


main = do
   [m,s]<-getArgs
   --stepBatch $ read s
   case (read s,m) of
      (x,"all")     -> stepAll   x
      (x,"step")    -> stepBatch x
      


   
   
--listDirectory framesDir

emptyRenderContext = RenderContext {
    _f   = ""
   ,_fs  = ""
   ,_fd  = ""
   ,_fdh = ""
   ,_ft  = ""
   ,_waitRetriesLeft = 100
   }





renderSingleFrame = do
   c<- St.get 
   liftIO $ putStrLn $ c^.f
   liftIO $ runCommand $ renderFrame $ c^.fs
   --m<-moveNewFrame         
   waitForFrame moveNewFrame 
   

waitForFrame :: (StateT RenderContext IO ()) -> StateT RenderContext IO ()
waitForFrame next = do 
   c<- St.get 
   case (c^.waitRetriesLeft>0)of
      False -> do 
                  liftIO $ putStrLn $ "wait time expired. Retrying for " ++ (c^.ft)
                  L.assign waitRetriesLeft 100
                  renderSingleFrame
      True  -> do
                  --l<-liftIO $ listDirectory tmpfsDir
                  e<- liftIO $ doesFileExist (c^.ft)
                  ep<-liftIO $ doesFileExist $ (c^.ft) ++ ".part"
                  case (e,ep) of
                     (True,False) -> waitTillStopsGrowing False 0 next
                     _ -> do
                        liftIO $ sleep 0.1
                        L.assign waitRetriesLeft $ (c^.waitRetriesLeft)-1
                        waitForFrame next


waitTillStopsGrowing :: Bool
                           -> Integer
                           -> (StateT RenderContext IO ()) 
                           -> StateT RenderContext IO ()
waitTillStopsGrowing lastCheck size next = do
   c<- St.get
   e<- liftIO $ doesFileExist (c^.ft)
   case e of
      False -> do 
                  liftIO $ putStrLn $ (c^.ft) ++ " doesn't exist. Waiting for it to emerge"
                  waitForFrame next
      True  -> do
                  s<- liftIO $ getFileSize (c^.ft)
                  case (s>size,lastCheck) of
                           (True,_) -> do 
                              liftIO $ sleep 0.1
                              waitTillStopsGrowing False s next
                           (False,True) -> next
                           (False,False)-> do
                              case (map (\t-> t s) testRanges) of
                                 
                                 [True,_,_]-> do  
                                                   liftIO $ sleep 0.1
                                                   waitTillStopsGrowing False s next
                                 _         -> do
                                                   liftIO $ sleep 0.1
                                                   waitTillStopsGrowing True s next
                                 {-
                                 [False,False,False]-> do  
                                                   liftIO $ sleep 1
                                                   waitTillStopsGrowing True s next
                                 [False,True,False]-> do  
                                                   liftIO $ sleep 0.5
                                                   waitTillStopsGrowing True s next
                                 [False,True,True] -> do
                                                   liftIO $ sleep 0.1
                                                   waitTillStopsGrowing True s next
                                 -}
      where
         testRanges =   [
                         (==0)
                        ,(>5000000)
                        ,(>6000000)
                        ]


moveNewFrame = do
   c<- St.get 
   
   --liftIO $ renamePath (c^.ft) (c^.fd)
   liftIO $ copyFile   (c^.ft) (c^.fd)
   liftIO $ (runCommand $ "rm " ++ tmpfsDir ++ "/*" 
                  )>>= waitForProcess
   return ()
                  
   {-e<- liftIO $ doesFileExist (c^.ft)
   case e of
      False -> return ()
      True  -> liftIO $ removeFile (c^.ft)
    -}           




renderBatch start = do 
   --runSteps [start, start+1 .. start+batchN-1]
   mapM_ step [start, start+1 .. start+batchN-1]
   where
   step n = evalStateT renderSingleFrame 
               $ set fs  (toFilenameSrv  n)
               $ set fdh (toFilenameHTML n)
               $ set ft  (toFilenameT    n)
               $ set fd  (toFilenameD    n)
               $ set f   (toFilename     n) emptyRenderContext
               
   runSteps [] = return ()
   runSteps (n:rest) = do 
      step n
      runSteps rest

encodeBatch start = do 
   putStrLn $ show start
   l<-listDirectory framesDir
   runCommandIfHasBatch start batch $ hasBatch l $ batch
   
   where
   batch = map toFilename [start, start+1 .. start+batchN-1]


stepBatch start = do 
   (runCommand $ "rm " ++ tmpfsDir ++ "/*" 
      )>>= waitForProcess
   renderBatch start
   encodeBatch start

stepAll start = do 
   l<-listDirectory framesDir
   writeFile "commandRender.sh" $ unlines 
      $ map c 
   --   $ map n_o_thml
      $ b
      $ maximum
      $ map o_n
      $ filter o_html l
      
   (runCommand $ "bash commandRender.sh")>>= waitForProcess
   return ()
   where
      c x = "runhaskell commandRender.hs step " ++ (show x)
      o_html x = ("o" `isPrefixOf` x) && (".html" `isSuffixOf` x) 
      o_n    x = read $ reverse $ drop 5 $ reverse $ drop 1 x
      n_o_thml n = "o" ++ (show n) ++ ".html"
      b max = [start,start+batchN.. max]


toFilename  n = nameBase ++ (show n) ++ ext
toFilenameD n = framesDir ++ "/" ++ nameBase ++ (show n) ++ ext
toFilenameT n = tmpfsDir  ++ "/" ++ nameBase ++ (show n) ++ ext
toFilenameHTML n = framesDir ++ "/" ++ nameBase ++ (show n) ++ ".html"
toFilenameSrv  n = nameBase ++ (show n) ++ ".html"

hasBatch l b = and $ map (\f-> elem f l) b

--hasMissing :: [FilePath] -> [(String,String,String)]-> [(String,String,String)]
hasMissing l b = filter (\(fs,f1,f,fh)-> isNothing $ find (isElem f) l) b
   where
   --_2 (_,f1,_) = f1
   isElem f1 f = f==f1

hasListed l b = filter (\(fs,f1,f,fh)-> isJust $ find (isElem f) l) b
   where
   --_2 (_,f1,_) = f1
   isElem f1 f = f==f1


reportMissing start = do
   putStrLn $ show start
   l<-listDirectory framesDir
   putStrLn $ unlines $ map _x $ hasMissing l batch
   return $ hasMissing l batch
   where
      batch = missingBatch start
      _x (_,x,_,_) = x
   
   
missingBatch start = zip4 (map toFilenameSrv  $ missingSequence start) 
                          (map toFilenameD    $ missingSequence start)
                          (map toFilename     $ missingSequence start)
                          (map toFilenameHTML $ missingSequence start)
missingSequence start = [start, start+1 .. start+batchN-1]


runFixMissing m = mapM_ (\(fs,f1,f,fh) -> do 
                                          --   applyStop fh 
                                             runRender fs f1
                                             sleep 5) m 


fixMissing start m = do
  -- m<-reportMissing start
   runFixMissing m 

fixIfHasMissing :: Int -> [(String,String,String,String)] -> IO ()
fixIfHasMissing start m = do
   case hadRunFullLength of
      False -> do 
                  putStrLn ("has " ++ (show $ length m) ++ " files to go")
      True  -> do 
                  runFixMissing m
   where
      end = start+batchN-1
      fstart = toFilename start
      fend   = toFilename end
      hadRunFullLength = (\l-> l==[]) 
                           $ hasListed [fstart,fend] m
      
      
                  
   


runCommandIfHasBatch start batch True = do
   (runCommand $ command s e ("t" ++ s ++ ".mkv")
      )>>= waitForProcess
      
   mapM_ removeFile $ map (\f-> framesDir ++ "/" ++ f) batch
   
   --(runCommand $ "rm " ++ tmpfsDir ++ "/o*.png" 
   --               )>>= waitForProcess
   
   --stepBatch (start+batchN)
   
   where
   s= show   start
   --e= show $ start+batchN
   e= show batchN

runCommandIfHasBatch start _ False = do
   runRender (toFilenameSrv start) (toFilenameD start)
   sleep 60
   
   m<-reportMissing start
   fixIfHasMissing start m
   
   encodeBatch start
   

runRender fs f1  = do 
   e<-doesFileExist f1
   case e of
      True -> putStrLn $ "runRender file exists: " ++ f1
      False -> do 
                  putStrLn $ "runRender for: " ++ fs
                  (runCommand $ renderFrame fs
                     ) -- >>= waitForProcess
                  return ()





sleep t = (runCommand $ "sleep " ++ (show t)
            )>>= waitForProcess



