{-# LANGUAGE TemplateHaskell #-}

import Control.Monad.RWS.Lazy
import Control.Monad.Except
--import Control.Monad.Extra
--import Data.DList hiding (tail,map,empty,fromList,head,concat,lookup)
--import qualified Data.DList as DL
import Control.Lens hiding ((.=),Context,(|>))
import qualified Control.Lens as L --hiding (element)


data Config =
   Cf { 
       _outputWidth :: Word
      ,_outputHeight :: Word
      ,_files :: [String]
      ,_verbosity ::[String]
      ,_outputFileBase :: String
     }deriving (Show)
L.makeLenses ''Config

data Context =
   C { 
       _cverbosity ::[String]
      ,_outputFile :: String
     }deriving (Show)
L.makeLenses ''Context

data ApplicationLog =
    OtherError       String
   |Note             String
   deriving (Show)




type A' m = RWST Config ([ApplicationLog]) Context m
type A''= RWST Config ([ApplicationLog]) Context 
--type A = A' Identity
--type AS = A' ()
type AIO = A' IO

newtype AT m a = A {
   runA :: RWST Config ([ApplicationLog]) Context m a
   }



--runA_ :: A a -> Config -> Context -> (a,[ApplicationLog],Context)
--runA_ a cf c =  runA a cf c

instance Monad m a => Monad (AT m a) where
   return = AT m a
   x >>= f = AT $ do 
               u<- runRWST x
               



a :: A String
a = do 
   c<-get 
   return $ "verbosity " ++ (unwords $ c^.cverbosity)
   
   
aio :: AIO ()
aio = do
   cf<-ask
   c <-get
   let (aa,_,_) = runRWS a cf c
   --aa<- lift a
   
   liftIO $ putStrLn aa
   
   
   return () 


main = do
   let cf = Cf { 
       _outputWidth = 1
      ,_outputHeight = 1
      ,_outputFileBase = "0"
      ,_files = []
      ,_verbosity = ["all"]
     }
     
   let c = C { 
       _cverbosity = ["sss"]
      ,_outputFile = []
     }
   evalRWST aio cf c




